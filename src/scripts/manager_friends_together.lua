--local fDefaultSort;

function onInit()
    --fDefaultSort = CombatManager.getCustomSort();
    --CombatManager.setCustomSort(sortfuncFriends);

    CombatManager.addCombatantFieldChangeHandler("initresult", "onUpdate", onUpdateInitResult);
end

function applyFollowInitiativeEffect(nodeFollower, nodeLeader, nInitiative)
    local sLeaderName = DB.getValue(nodeLeader, "name", ""):upper();

    for _, vEffect in pairs(DB.getChildren(nodeFollower, "effects")) do
        local bIsActive = DB.getValue(vEffect, "isactive", 0);

        if bIsActive == 1 then
            local sEffectLabel = DB.getValue(vEffect, "label", "");
            local tEffect = EffectManager.parseEffectCompSimple(sEffectLabel);

            if tEffect.type == "INIT" then
                local tInitiativeMod = self.findInitiativeMod(tEffect.original, sLeaderName, nInitiative);

                if tInitiativeMod.type ~= "none" then
                    DB.setValue(nodeFollower, "initresult", "number", tInitiativeMod.result);
                end
            end
        end
    end
end

function findInitiativeMod(sEffect, sLabelMatch, nInitiative)
    sEffect = sEffect:upper();

    local tResults = {};
    tResults.type = "none";

    if sEffect:find("FOLLOW(" .. sLabelMatch .. ")", 1, true) ~= nil then
        tResults.type = "decrease";
        tResults.result = nInitiative-- - 0.01;
    elseif sEffect:find("PRECEDE(" .. sLabelMatch .. ")", 1, true) ~= nil then
        tResults.type = "increase";
        tResults.result = nInitiative-- + 0.01;
    end

    return tResults;
end

--function findInitiativeEffect(nodeSource, nodeFollower)
--    local tResults = {};
--    tResults.type = "none";
--
--    local sLeaderName = DB.getValue(nodeSource, "name", ""):upper();
--    local aEffects = EffectManager.getEffectsByType(nodeFollower, "INIT");
--
--    if #aEffects == 0 or sLeaderName == "" then
--        return tResults;
--    end
--
--    local sEffect = "";
--
--    for _, rEffect in ipairs(aEffects) do
--        sEffect = rEffect.original; -- take last "INIT" effect
--    end
--
--    sEffect = sEffect:upper();
--
--    if sEffect:find("FOLLOW(" .. sLeaderName .. ")", 1, true) ~= nil then
--        tResults.type = "follows";
--    elseif sEffect:find("PRECEDE(" .. sLeaderName .. ")", 1, true) ~= nil then
--        tResults.type = "precedes";
--    end
--
--    return tResults;
--end

function onUpdateInitResult(nodeField)
    local nodeCT = nodeField.getParent();
    local nInitResult = DB.getValue(nodeCT, "initresult");

    if nInitResult == nil or nInitResult == 0 then
        return
    end

    for _, nodeOther in pairs(CombatManager.getCombatantNodes()) do
        if nodeOther ~= nil and nodeOther ~= nodeCT then
            self.applyFollowInitiativeEffect(nodeOther, nodeCT, nInitResult);
        end
    end
end

--function sortfuncFriends(node1, node2)
--    local tEffectA = self.findInitiativeEffect(node1, node2);
--    local tEffectB = self.findInitiativeEffect(node2, node1);
--
--    if tEffectA.type == "follows" or tEffectB.type == "precedes" then
--        return true;
--    elseif tEffectA.type == "precedes" or tEffectB.type == "follows" then
--        return false;
--    end
--
--    return fDefaultSort(node1, node2);
--end
