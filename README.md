# FGU Friends Together

Link character initiatives for familiars and friends!

This extension adds support for the following effects:

| Effect                      | Result                                                                                                     |
|-----------------------------|------------------------------------------------------------------------------------------------------------|
| `INIT: FOLLOW(Actor Name)`  | When `Actor Name's` initiative changes, set this one's initiative to the same, minus 0.1 ("follow behind") |
| `INIT: PRECEDE(Actor Name)` | When `Actor Name's` initiative changes, set this one's initiative to the same, plus 0.1 ("follow ahead")   |
